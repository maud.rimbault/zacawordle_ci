FROM eclipse-temurin:17
WORKDIR /application
COPY . .
RUN ./mvnw package
ENTRYPOINT ["java", "-jar", "target/wordle-1.0-SNAPSHOT.jar"]