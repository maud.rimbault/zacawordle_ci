package com.zenika.academy.barbajavas.wordle.web;

public record GuessRequestDto(String guess) {
}
